using System;
using Xunit;

using IngenDNA;

namespace DNATest
{
    public class DNATest
    {
        [Fact]
        public void TestIsValid()
        {
            Assert.True(DNA.IsValid("ATC"));
            Assert.True(DNA.IsValid("ATCGAC"));
            Assert.False(DNA.IsValid("ATCG"));
            Assert.False(DNA.IsValid("AXCEEF"));
        }


        [Fact]
        public void TestComplementaryStrand()
        {
            Assert.Equal("TAG", DNA.ComplementaryStrand("ATC"));
            Assert.Equal("TAGCTG", DNA.ComplementaryStrand("ATCGAC"));
            Assert.Null(DNA.ComplementaryStrand("ATCG"));
            Assert.Null(DNA.ComplementaryStrand("AXCEEF"));
        }

        [Fact]
        public void TestHowManyCodons()
        {
            Assert.Equal(1, DNA.HowManyCodons("ATC"));
            Assert.Equal(2, DNA.HowManyCodons("ATCGAC"));
            Assert.Equal(0, DNA.HowManyCodons("ATCG"));
            Assert.Equal(0, DNA.HowManyCodons("AXCEEF"));
        }
    }
}
