﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace IngenDNA
{
    public class DNA
    {

        public static char[] bases = {'A', 'T', 'C', 'G'};
        public static Dictionary<char, char> complementaries =  new Dictionary<char, char>();

        static DNA() {
            complementaries.Add('A', 'T');
            complementaries.Add('T', 'A');
            complementaries.Add('C', 'G');
            complementaries.Add('G', 'C');
        }
        

        public static bool IsValid(String dna)
        {
            

            if (dna.Length % 3 != 0)
            {
                return false;
            }

            bool result = true;

            foreach(char base_az in dna)
            {
                if(!DNA.bases.Contains(base_az))
                {
                    result = false;
                }
            }

            return result;
        }

        public static String ComplementaryStrand(String dna)
        {
            if(DNA.IsValid(dna)){
                String result = String.Empty;
                foreach(char base_az in dna)
                {
                    char complementary_base = DNA.complementaries[base_az];
                    result = String.Format("{0}{1}", result, complementary_base);
                }
                return result;
            }
            return null;
        }

        public static int HowManyCodons(String dna)
        {
            if(DNA.IsValid(dna))
            {
                return dna.Length / 3;
            }   
            return 0;
        }
    }
}
